yii2-user
=========

User module with inbuilt social authentication for Yii framework 2.0.

Refer the [design skeleton](https://github.com/communityii/yii2-user/blob/master/docs/DESIGN.md) for design discussion and planned features.

> NOTE: This module is under development and not ready for an alpha release yet.